(ns anglican.delay
  "Runtime functions for delayed sampling"
  (:require [anglican.runtime :refer [sqrt abs normal
                                      beta bernoulli]]
            [anglican.trap :refer [->sample ->observe]]
            [clojure.pprint :refer [pprint]]))

;;;; Debugging

(def ^:private debug false)

(defn- debug-print
  "Print one or more objects if debug = false.
  If there is only one object, use pprint."
  ([object] (if debug (pprint object)))
  ([object & objects] (if debug (apply println object objects))))

;;;; Helpers

(defn- eval-dist
  "Evaluate the distribution encoded by
  a :data entry in a delayed variate map"
  [{dist :dist, params :params}]
  (apply dist params))

(defn- dv?
  "Check if a symbol is a delayed variate"
  [symb]
  (= (type symb) ::dv))

(defn- new-dv
  "Generate a new dv identifier based on the number
  of other delayed variates in the state."
  [$state]
  (with-meta (symbol (str "DV_" (count (::delay $state))))
             {:type ::dv}))

(defn- data-init?
  "Check if a :data entry is initialized"
  [{dist :dist, params :params}]
  (and dist (not params)))

(defn- data-marg?
  "Check if a :data entry is marginalized"
  [{dist :dist, params :params}]
  (and dist params))

(defn- data-rea?
  "Check if a :data entry is realized"
  [data]
  (number? data))

;;; Modify and query state

(defn- get-node
  "Get a node from a state by the given delayed variate id."
  [$state id]
  (get-in $state [::delay id]))

(defn- get-data
  "Get the data for a node from a state by the given delayed variate id."
  [$state id]
  (get-in $state [::delay id :data]))

(defn- add-edge
  "Add an edge pid -> id from the state"
  [$state pid id]
  (-> $state
      (#(if (get-in % [::delay pid :cids])
          (update-in % [::delay pid :cids] conj id)
          (assoc-in % [::delay pid :cids] #{id})))
      (assoc-in [::delay id :pid] pid)))

(defn- remove-edge
  "Remove an edge pid -> id from the state"
  [$state pid id]
  (-> $state
      (#(if (= (count (get-in % [::delay pid :cids])) 1)
          (update-in % [::delay pid] dissoc :cids)
          (update-in % [::delay pid :cids] disj id)))
      (update-in [::delay id] dissoc :pid)))

(defn- add-node
  "Add a new node to the state."
  ([$state id checkpoint pid dist marg cond]
   (-> $state
       (assoc-in [::delay id]
                 {:data {:dist dist},
                  :marg marg,
                  :cond cond,
                  :checkpoint checkpoint})
       (add-edge pid id)))
  ([$state id checkpoint dist params]
   (assoc-in $state
             [::delay id]
             {:data {:dist dist,
                     :params params},
              :checkpoint checkpoint})))

(defn- set-data
  "Set the :data entry for a node."
  ([$state id data]
   (assoc-in $state [::delay id :data] data)))

(defn- remove-checkpoint
  "Remove the checkpoint for a node."
  [$state id]
  (update-in $state [::delay id] dissoc :checkpoint))

(defn- marginalize
  "Marginalize a node in the state, given its delayed variate id."
  [$state id]
  (debug-print "--")
  (debug-print "marginalize" id)
  (debug-print (get-in $state [::delay]))
  (let [node (get-node $state id)
        pid (:pid node)
        pnode (get-node $state pid)
        fm (:marg node)
        data (fm (:data pnode))]
    (-> $state
        (update-in [::delay id] dissoc :marg)
        (set-data id data))))

(defn- detach
  "Detach a node in the state, given its delayed variate id."
  [$state id]
  (debug-print "--")
  (debug-print "detach" id)
  (debug-print (get-in $state [::delay]))
  (reduce
    (fn [$state cid]
      (-> $state
          (marginalize cid)
          (remove-edge id cid)
          (update-in [::delay cid] dissoc :cond)))
    $state
    (:cids (get-node $state id))))

(defn- condition
  "Condition a node in the state, given its delayed variate id."
  [$state id]
  (debug-print "--")
  (debug-print "condition" id)
  (debug-print (get-in $state [::delay]))
  (let [node (get-node $state id)
        pid (:pid node)]
    (if pid
      (let [pnode (get-node $state pid)
            fc (:cond node)
            data (fc (:data pnode) (get-data $state id))]
        (-> $state
            (update-in [::delay id] dissoc :cond)
            (set-data pid data)
            (remove-edge pid id)))
      $state)))

(defn- set-mcid
  "Set the mcid of a node in the state, given its delayed variate id."
  [$state id mcid]
  (assoc-in $state [::delay id :mcid] mcid))

(defn- remove-mcid
  "Remove the mcid for a node in the state, given its delayed variate id."
  [$state id]
  (if id
    (update-in $state [::delay id] dissoc :mcid)
    $state))

(defn- isolate-node
  "Isolate a node in the state, given its delayed variate id and a value for
  it. Used as the last step in the ds-value function."
  [$state id value]
  (debug-print "--")
  (debug-print "isolate-node" id value)
  (debug-print (get-in $state [::delay]))
  (-> $state
      (set-data id value)
      (remove-checkpoint id)
      (detach id)
      (condition id)
      (remove-mcid (:pid (get-node $state id)))))

;;;; Closed-form solutions

;;; Normal with normal mean

(defn- nnm-marg
  "Marginalize a normal distribution given a normal distribution for its
  parent"
  [sigma mean-scale mean-offset]
  (fn [data]
    {:dist normal,
     :params
     (cond
       ;; If parent is a normal distribution
       (= (:dist data) normal)
       (let [[mu_0 sigma_0] (:params data)

             ;; Do the linear transformation
             mu_0    (+ (* mu_0 mean-scale) mean-offset)
             sigma_0 (* sigma_0 (abs mean-scale))]

         [mu_0 (sqrt (+ (* sigma_0 sigma_0)
                        (* sigma sigma)))])

       ;; If parent is a value
       (number? data)
       [(+ (* data mean-scale) mean-offset) sigma]

       :else
       (assert false "Error in nnm-marg"))}))

(defn- nnm-cond
  "Condition the value of a normally distributed child onto its normally
  distributed parent."
  [sigma mean-scale mean-offset]
  (fn [data value]
    (cond
      ;; If parent is a normal distribution
      (= (:dist data) normal)
      {:dist normal,
       :params
       (let [[mu_0 sigma_0] (:params data)

             ;; Do the linear transformation
             mu_0    (+ (* mu_0 mean-scale) mean-offset)
             sigma_0 (* sigma_0 (abs mean-scale))

             ;; Update the transformed normal
             denom (+ (/ (* sigma_0 sigma_0))
                      (/ (* sigma sigma)))
             mu_0  (/ (+ (/ mu_0  (* sigma_0 sigma_0))
                         (/ value (* sigma sigma))) denom)
             sigma_0 (sqrt (/ denom))

             ;; Transform back (inverse transformation)
             mu_0    (- (/ mu_0 mean-scale) mean-offset)
             sigma_0 (/ sigma_0 (abs mean-scale))]
         [mu_0 sigma_0])}

      :else
      (assert false "Error in nnm-cond"))))

;;; Beta-Bernoulli

(defn- bern-marg
  "Marginalize a bernoulli distribution given a beta distribution for its
  parent"
  []
  (fn [data]
    {:dist bernoulli,
     :params
     (cond
       ;; If parent is a beta distribution
       (= (:dist data) beta)
       (let [[a b] (:params data)]
         [(/ a (+ a b))])

       ;; If parent is a value
       (number? data)
       [data]

       :else
       (assert false "Error in bern-marg"))}))

(defn- bern-cond
  "Condition the value of a bernoulli distributed child onto its beta
  distributed parent."
  []
  (fn [data value]
    (cond
      ;; If parent is a beta distribution
      (= (:dist data) beta)
      {:dist beta,
       :params
       (let [[a b] (:params data)]
         [(+ a value) (+ b (- 1 value))])}

      :else
      (assert false "Error in nnm-cond"))))

;;;; CPS functions (returns ->sample, ->observe or thunk)

;;; Operations on delayed variates

(declare dist)

(defn ds-value
  "Get a value for a delayed variate."
  [cont $state arg]
  (debug-print "-------------------")
  (debug-print "ds-value" arg)
  (debug-print (get-in $state [::delay]))
  (if (dv? arg)

    ;; If it is a delayed variate
    (if (data-rea? (get-data $state arg))

      ;; If it is realized
      (fn [] (cont (get-data $state arg) $state))

      ;; If it is initialized or marginalized
      (dist
        (fn [$state]
          (->sample
            (:checkpoint (get-node $state arg))
            (eval-dist (get-data $state arg))
            (fn [value $state]
              (fn [] (cont value
                           (isolate-node $state arg value))))
            $state))
        $state
        arg))

    ;; Simply return arg if not a delayed variate
    (fn [] (cont arg $state))))

(defn ds-observe
  "Observe a value for a delayed variate."
  [cont $state checkpoint arg value]
  (debug-print "-------------------")
  (debug-print "observe" arg value)
  (debug-print (get-in $state [::delay]))
  (if (dv? arg)
    (do
      (or (not (data-rea? (get-data $state arg)))
          (throw (java.lang.IllegalArgumentException.
                   "Invalid dv for observe")))
      (or (number? value)
          (throw (java.lang.IllegalArgumentException.
                   "Invalid value for observe")))
      (dist
        (fn [$state]
          (->observe
            (:checkpoint (get-node $state arg))
            (eval-dist (get-data $state arg))
            value
            (fn [r $state]
              (fn [] (cont r (isolate-node $state arg value))))
            $state))
        $state
        arg))

    (->observe checkpoint arg value cont $state)))

(defn- dist
  "Ensure that a delayed variate is marginalized, and that it has no other
  marginalized child."
  [cont $state arg]
  (debug-print "--")
  (debug-print "dist" arg)
  (debug-print (get-in $state [::delay]))
  (if (data-init? (get-data $state arg))

    ;; It has not been marginalized: ensure a distribution for
    ;; parent, then marginalize
    (dist (fn [$state]
            (fn [] (cont (-> $state
                             (marginalize arg)
                             (set-mcid (:pid (get-node $state arg))
                                       arg)))))
          $state
          (:pid (get-node $state arg)))

    ;; It has already been marginalized, retract
    (if (:mcid (get-node $state arg))
      (ds-value (fn [_ $state] (fn [] (cont $state)))
             $state
             (:mcid (get-node $state arg)))

      ;; If no marginalized child, return
      (fn [] (cont $state)))))

;;; Creating delayed variates

(defn ds-bernoulli
  "Create a Bernoulli distributed delayed variate."
  [cont $state checkpoint prob]
  (debug-print "-------------------")
  (debug-print "ds-bernoulli:")
  (debug-print (get-in $state [::delay]))
  (or (dv? prob) (number? prob)
      (throw (java.lang.IllegalArgumentException.
               "Invalid prob")))

  (let [p (or (get-data $state prob) prob)
        dv (new-dv $state)]
    (cond
      ;; Analytical relationship
      (= (:dist p) beta)
      (fn []
        (cont dv
              (add-node $state dv checkpoint
                        prob bernoulli
                        (bern-marg)
                        (bern-cond))))

      ;; No analytical relationship but still dist
      (:dist p)
      (ds-value #(ds-bernoulli cont %2 checkpoint prob) $state prob)

      ;; prob is a number
      :else
      (fn []
        (cont dv (add-node $state dv
                           checkpoint bernoulli
                           [p]))))))

(defn ds-beta
  "Create a beta distributed delayed variate."
  [cont $state checkpoint shape1 shape2]
  (debug-print "-------------------")
  (debug-print "ds-beta:")
  (debug-print (get-in $state [::delay]))
  (or (dv? shape1) (number? shape1)
      (throw (java.lang.IllegalArgumentException.
               "Invalid shape1")))

  (or (dv? shape2) (number? shape2)
      (throw (java.lang.IllegalArgumentException.
               "Invalid shape2")))

  (let [a (or (get-data $state shape1) shape1)
        b (or (get-data $state shape2) shape2)
        dv (new-dv $state)]
    (cond
      ;; No analytical relationships for the beta distribution
      (:dist a)
      (ds-value #(ds-beta cont %2 checkpoint shape1 shape2)
             $state shape1)
      (:dist b)
      (ds-value #(ds-beta cont %2 checkpoint shape1 shape2)
             $state shape2)
      :else
      (fn []
        (cont dv (add-node $state dv
                           checkpoint beta
                           [a b]))))))

(defn ds-normal
  "Create a normally distributed delayed variate."
  ;; Supply neither scale nor offset
  ([cont $state checkpoint                        mean sd]
   (ds-normal cont $state checkpoint 1 0 mean sd))

  ;; Supply scale
  ([cont $state checkpoint mean-scale             mean sd]
   (ds-normal cont $state checkpoint mean-scale 0 mean sd))

  ;; Supply both scale and offset
  ([cont $state checkpoint mean-scale mean-offset mean sd]
   (debug-print "-------------------")
   (debug-print "ds-normal:")
   (debug-print (get-in $state [::delay]))
   (or (dv? mean) (number? mean)
       (throw (java.lang.IllegalArgumentException.
                "Invalid mean")))

   (or (dv? sd) (number? sd)
       (throw (java.lang.IllegalArgumentException.
                "Invalid standard deviation")))

   (let [mu (or (get-data $state mean) mean)
         sigma (or (get-data $state sd) sd)
         dv (new-dv $state)]
     (cond

       ;; sigma must be realized or a number
       (:dist sigma)
       (ds-value #(ds-normal cont %2 checkpoint mean sd) $state sd)

       ;; Analytical relationship
       (= (:dist mu) normal)
       (fn []
         (cont dv
               (add-node $state dv checkpoint
                         mean normal
                         (nnm-marg sigma mean-scale mean-offset)
                         (nnm-cond sigma mean-scale mean-offset))))

       ;; No analytical relationship, mean must
       ;; be realized or a number
       (:dist mu)
       (ds-value #(ds-normal cont %2 checkpoint mean sd) $state mean)

       ;; mean and sd both numbers
       :else
       (fn []
         (cont dv (add-node $state dv
                            checkpoint normal
                            [(+ (* mu mean-scale) mean-offset)
                             (* sigma (abs mean-scale))])))))))

