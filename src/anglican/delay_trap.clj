(ns anglican.delay-trap
  "Transformations for delayed sampling"
  (:require [anglican.trap :refer [*gensym*]]))

;; All ds-functions to be expanded with checkpoint addresses
(def dvs '#{ds-normal ds-bernoulli ds-beta})

(declare delay-transform)

(defn delay-transform-ds-observe
  "Add a checkpoint address to a ds-observe call.
  If one is provided, do nothing."
  [args]
  (let [args (if (= (count args) 2)
               `['~(*gensym* "O") ~@args]
               args)]
    `(~'anglican.delay/ds-observe
       ~@(map delay-transform args))))

(defn delay-transform
  "Transform an expression to enable delayed sampling"
  [expr]
  (cond
    (symbol? expr) expr
    (vector? expr) (apply vector (map delay-transform expr))
    (map? expr)    (into {} (map delay-transform expr))
    (set? expr)    (set (map delay-transform expr))
    (seq? expr)
    (let [[kwd & rest] expr]
      (if (dvs kwd)
        `(~kwd '~(*gensym* "DS") ~@(map delay-transform rest))
        (case kwd
          ;; We require special handling for ds-observe
          ds-observe    (delay-transform-ds-observe rest)
          quote      expr
          `(~kwd ~@(map delay-transform rest)))))
    :else expr))

